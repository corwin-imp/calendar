/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {Provider} from 'react-redux';
import HomeScreen from './screens/HomeScreen';

import store from './store/configureStore';
import DayScreen from './screens/DayScreen';
import AddExerciseScreen from './screens/AddScreen';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
const MainNavigator = createStackNavigator({
  Home: HomeScreen,

  AddExercise: AddExerciseScreen,
  Day: {
    screen: DayScreen,
    navigationOptions: ({navigation}) => {

      return {
        day: navigation.state.params.day,
        dayId: navigation.state.params.dayId,
      };
    },
  },
});
const NavigationContainer = createAppContainer(MainNavigator);

const NavigationApp = () => (
  <Provider store={store}>
    <NavigationContainer />
  </Provider>
);

export default NavigationApp;
