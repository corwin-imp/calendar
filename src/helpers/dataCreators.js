import moment from 'moment';

const exercise = [
  {
    exType: 'Press',
    countEx: 3,
    id: 33222,
    exDouble: true,
  },
  {
    exType: 'PushUps',
    countEx: 3,
    id: 3322,
    exDouble: false,
  },
  {
    exType: 'PullUps',
    countEx: 4,
    id: 332,
    exDouble: true,
  },
  {
    exType: 'ShuttleRun',
    countEx: 2,
    id: 32,
    exDouble: true,
  },
];

export const dateCreators = count => {
  const dateObj = {};

  for (let i = 0; i < count; i++) {
    let dayDate = moment()
      .startOf('isoWeek')
      .add(i, 'day');
    let dayName = dayDate.format('ddd');
    let dayId = dayDate.format('DD-MM-YYYY');

    dateObj[`${i}id_${dayId}`] = {
      id: `${i}id_${dayId}`,
      dayName: dayName,
      dayDate: dayId,
      exercise: [],
    };
  }
  dateObj[
    `0id_${moment()
      .startOf('isoWeek')
      .format('DD-MM-YYYY')}`
  ].exercise = exercise;

  return dateObj;
};
