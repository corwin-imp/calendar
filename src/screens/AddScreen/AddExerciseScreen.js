import React from 'react';
import Icon from 'react-native-ionicons/index';
import {Field} from 'redux-form';

import {
  Container,
  DatePicker,
  Header,
  Content,
  Form,
  Item,
  Button,
  CheckBox,
  ListItem,
  Body,
  Text,
  Input,
  Picker,
} from 'native-base';

function getMonday(d) {
  d = new Date(d);
  let day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}
const Monday = getMonday(new Date());

class AddExerciseScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined,
      chosenDate: new Date(),
    };
    this.renderInput = this.renderInput.bind(this);
    this.renderSelect = this.renderSelect.bind(this);
    this.renderCheck = this.renderCheck.bind(this);
    this.renderDate = this.renderDate.bind(this);
    this.setDate = this.setDate.bind(this);
  }

  static navigationOptions = {
    title: 'AddExercise',
  };
  setDate(newDate, input) {

    return input.onChange(newDate);
  }
  onValueChange2 = (value, input) => {
    return input.onChange(value);
  };

  renderInput({input, label, type, meta: {touched, error, warning}}) {
    let hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <Item error={hasError}>
        <Input type={type} {...input} />
        {hasError ? <Text>{error}</Text> : <Text />}
      </Item>
    );
  }
  renderCheck({input, label, type, meta: {touched, error, warning}}) {
    let hasError = false;
    if (error !== undefined) {
      hasError = true;
    }

    return (
      <Item error={hasError}>
        <CheckBox
          {...input}
          checked={!!input.value}
          onPress={() => input.onChange(!input.value)}
        />
        {hasError ? <Text>{error}</Text> : <Text />}
      </Item>
    );
  }
  renderDate({input, label, type, meta: {touched, error, warning}}) {
    let hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    const {date} = this.props;
    return (
      <Item error={hasError}>
        <DatePicker
          {...input}
          defaultDate={new Date()}
          minimumDate={Monday}
          maximumDate={new Date(2019, 12, 31)}
          locale={'en'}
          timeZoneOffsetInMinutes={undefined}
          modalTransparent={false}
          animationType={'fade'}
          androidMode={'default'}
          placeHolderText="Select date"
          textStyle={{color: 'green'}}
          placeHolderTextStyle={{color: '#d3d3d3'}}
          onDateChange={val => this.setDate(val, input)}
          disabled={false}
        />
        <Text>Date: {date.toString().substr(4, 12)}</Text>
        {hasError ? <Text>{error}</Text> : <Text />}
      </Item>
    );
  }
  renderSelect({input, label, type, meta: {touched, error, warning}}) {
    let hasError = false;
    if (error !== undefined) {
      hasError = true;
    }
    return (
      <Item error={hasError}>
        <Picker
          {...input}
          mode="dropdown"
          iosIcon={<Icon ios="ios-add" android="md-add" />}
          style={{width: undefined}}
          placeholder="Select your SIM"
          placeholderStyle={{color: '#bfc6ea'}}
          placeholderIconColor="#007aff"
          selectedValue={input.value}
          onValueChange={val => this.onValueChange2(val, input)}>
          <Picker.Item label="Bench Press" value="Press" />
          <Picker.Item label="Push ups" value="PushUps" />
          <Picker.Item label="Pull-ups" value="PullUps" />
          <Picker.Item label="Shuttle run" value="ShuttleRun" />
        </Picker>
        {hasError ? <Text>{error}</Text> : <Text />}
      </Item>
    );
  }
  render() {
    const {handleSubmit, reset} = this.props;
    return (
      <Container>
        <Header />
        <Content>
          <Form>
            <Field name="Date" component={this.renderDate} />
            <Item picker>
              <Field name="exType" component={this.renderSelect} />
            </Item>

            <Field name="countEx" type="number" component={this.renderInput} />
            <ListItem>
              <Field name="exDouble" component={this.renderCheck} />
              <Body>
                <Text>Daily Stand Up</Text>
              </Body>
            </ListItem>

            <Button block primary onPress={handleSubmit}>
              <Text>Submit</Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default AddExerciseScreen;
