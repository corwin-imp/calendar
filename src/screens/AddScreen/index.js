import {reduxForm, formValueSelector} from 'redux-form';
import {connect} from 'react-redux';
import {addExerciseAction} from '../../store/Exercises/ExercisesActions';
import AddExerciseScreen from './AddExerciseScreen';

const selector = formValueSelector('addExercise');

const SignInContainer = reduxForm({
  form: 'addExercise',
  onSubmit: (values, dispatch, props) => {

    dispatch(addExerciseAction(values));
    props.navigation.navigate('Home');
  },
})(AddExerciseScreen);

const mapStateToProps = state => {
  let date = selector(state, 'Date');
  date = date ? date : new Date()
  return {
    date,
  };
};

const dispatchToProps = dispatch => {
  return {
    //addExerciseAction: data => dispatch(addExerciseAction(data)),
  };
};

export default connect(
  mapStateToProps,
  dispatchToProps,
)(SignInContainer);
