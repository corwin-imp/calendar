import React, {Component} from 'react';
import {Button, FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {
  FooterTab,
  Header,
  Content,
  Footer,
  Container,
  Form,
  Picker,
} from 'native-base';
const imgs = {
  PullUps:
    'https://i1.wp.com/gasparinutrition.com/wp-content/uploads/2018/12/What-Muscles-Do-Pull-ups-Work.jpg?resize=1024%2C480&ssl=1',
  Press:
    'https://www.mensjournal.com/wp-content/uploads/mf/4.-high-pull-30-best-shoulder-exercises-of-all-time-shoulders.jpg?w=1200',
  PushUps:
    'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/08/main-0-1484243182.jpg?resize=480:*',
  ShuttleRun:
    'https://training365.ru/wp-content/uploads/2019/06/%D1%87%D0%B5%D0%BB%D0%BD%D0%BE%D1%87%D0%BD%D1%8B%D0%B9-%D0%B1%D0%B5%D0%B3-%D0%BD%D0%BE%D1%80%D0%BC%D0%B0%D1%82%D0%B8%D0%B2%D1%8B-%D1%82%D0%B5%D1%85%D0%BD%D0%B8%D0%BA%D0%B0.jpg',
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  img: {
    borderRadius: 50,
  },
  ccs: {
    flex: 1,
    flexDirection: 'row',
  },

  sectionContainer: {
    flex: 1,
  },
  btn: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    textAlign: 'center',
  },

  item: {
    textAlign: 'center',
    flex: 1,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    fontWeight: 'bold',
    borderBottomColor: 'white',
    color: '#111',
    padding: 0,
    fontSize: 10,
  },
});
const Card = ({title, src, exDouble, repeat, titleTh, navigate}) => {
  return (
    <View style={styles.item}>
      <Image
        source={{uri: imgs[src]}}
        style={{width: 100, borderRadius: 50, height: 100}}
      />
      <Text style={styles.titleSec}>{repeat}</Text>
      <Text style={styles.exDouble}>{exDouble ? 'double' : null}</Text>
      <Text style={styles.titleTh}>{titleTh}</Text>
    </View>
  );
};
class DayScreen extends React.Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: `${navigation.getParam('day', 'NO-ID')}`,
    };
  };

  render() {
    const {navigate} = this.props.navigation;
    const {navigation, date} = this.props;


    const dayId = navigation.getParam('dayId', 'NO-ID');
    const dayObj = date[dayId];

    return (
      <Container>
        <Content>
          <View>
            <FlatList
              data={dayObj.exercise}
              renderItem={({item}) => (
                <Card
                  navigate={navigate}
                  src={item.exType}
                  repeat={item.countEx}
                  exDouble={item.exDouble}
                  title={item.title}
                />
              )}
              keyExtractor={item => `${item.id}`}
            />
          </View>
        </Content>
        <Footer>
          <FooterTab style={styles.btn}>
            <Button
              title="Add Exercise"
              onPress={() => navigate('AddExercise')}
            />
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

export default DayScreen;
