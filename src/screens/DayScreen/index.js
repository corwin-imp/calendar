import {connect} from 'react-redux';
import DayScreen from './DayScreen';

const mapStateToProps = state => {
  return {
    date: state.exercises.date,
  }
}

const dispatchToProps = dispatch => {
  return {
    //addExerciseAction: data => dispatch(addExerciseAction(data)),
  };
};

export default connect(
  mapStateToProps,
  dispatchToProps,
)(DayScreen);
