import React, {Component} from 'react';
import {
  Button,
  Dimensions,
  View,
  FlatList,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

let {width} = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
  sectionHeader: {
    paddingTop: 2,

    height: 24,
    textAlign: 'center',
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 18,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
  section: {
    width: width,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    flex: 1,
    borderBottomWidth: 0,
    borderBottomColor: 'white',
  },
  btn: {
    fontSize: 10,
    padding: 0,
    height: 10,
  },
  item: {
    paddingLeft: 10,
    textAlign: 'center',
    flex: 1,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    fontWeight: 'bold',
    borderBottomColor: 'white',
    color: '#111',
    padding: 0,
    fontSize: 10,
  },
});
function Item({title, dayName, dayDate, id, exercise, navigate}) {
  return (
    <TouchableOpacity
      onPress={() => navigate('Day', {day: dayDate, dayId: id})}
      style={styles.item}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.titleSec}>{dayName}</Text>
      <Text style={styles.titleTh}>{`${exercise.length} exercises`}</Text>
      <Button
        title="View"
        containerStyle={styles.btn}
        onPress={() => navigate('Day', {day: dayDate, dayId: id})}
      />
    </TouchableOpacity>
  );
}

function Section({title, weekData, navigate}) {
  return (
    <View style={styles.section}>
      <Text style={styles.sectionHeader}>{title}</Text>
      <FlatList
        data={weekData}
        renderItem={({item}) => (
          <Item
            navigate={navigate}
            dayName={item.dayName}
            dayDate={item.dayDate}
            id={item.id}
            exercise={item.exercise}
            title={item.dayDate}
          />
        )}
        keyExtractor={item => item.id}
      />
    </View>
  );
}

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Home',
  };
  render() {
    const {navigate} = this.props.navigation;
    const {date} = this.props;


    return (
      <View style={styles.container}>
        <FlatList
          horizontal={true}
          data={date}
          renderItem={({item, index}) => (
            <Section
              navigate={navigate}
              weekData={Object.values(item)}
              title={`${+index + 1} Week`}
            />
          )}
          keyExtractor={(item, i) => `${i}-week`}
        />
      </View>
    );
  }
}

export default HomeScreen;
