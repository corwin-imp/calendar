import {connect} from 'react-redux';
import HomeScreen from './HomeScreen';

function splitTo(arr, n) {
  let plen = Math.ceil(arr.length / n);

  return arr.reduce(function(p, c, i, a) {
    if (i % plen === 0) {
      p.push({});
    }
    p[p.length - 1][i] = c;
    return p;
  }, []);
}

const mapStateToProps = state => {

  const date = {...state.exercises.date};
  let dateWeeks = splitTo(Object.values(date), 3);

  return {
    date: dateWeeks,
    //serverMessage: state.user.serverMessage,
    //serverError: state.user.serverError,
  };
};

const dispatchToProps = dispatch => {
  return {
    //addExerciseAction: data => dispatch(addExerciseAction(data)),
  };
};

export default connect(
  mapStateToProps,
  dispatchToProps,
)(HomeScreen);
