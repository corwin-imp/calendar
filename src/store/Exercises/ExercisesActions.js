import {createAction} from 'redux-actions';

export const addExerciseAction = createAction('ADD_EXERCISE', data => ({data}));
