import {handleActions, combineActions} from 'redux-actions';
import moment from 'moment';
import {addExerciseAction} from './ExercisesActions';
import {dateCreators} from '../../helpers/dataCreators';

const data = dateCreators(21);
const defaultState = {date: data};

const reducer = handleActions(
  {
    [addExerciseAction]: (state, action) => {
      let dateKey = moment(action.payload.data.Date).format('DD-MM-YYYY');
      let keyNum = Object.keys(state.date).find(id => id.includes(dateKey));

      let dateObj = {...state.date[keyNum]};
      delete action.payload.data.Date;

      dateObj.exercise.push({
        ...action.payload.data,
        id: dateObj.exercise.length,
      });
      return {
        ...state,
        date: {
          ...state.date,
          [keyNum]: {
            ...state.date[keyNum],
            exercise: dateObj.exercise,
          },
        },
      };
    },
  },
  defaultState,
);

export default reducer;
