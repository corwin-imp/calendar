import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import ExercisesReducer from './Exercises/ExercisesReducer';
const reducers = {
  form: formReducer,
  exercises: ExercisesReducer,
};
const allReducers = combineReducers(reducers);

export default allReducers;
