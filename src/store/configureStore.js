import {createStore} from 'redux';
import reducers from './RootReducer';
import devToolsEnhancer from 'remote-redux-devtools';

const initialState = {};

const store = createStore(
  reducers,
  initialState,
  devToolsEnhancer({realtime: true, port: 8081}),
);

export default store;
